package com.hackathon5a.moneyservice.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.hackathon5a.moneyservice.model.User;

@Repository
public class MySQLMoneyRequestRepository implements MoneyRequestRepository {

    @Autowired
    JdbcTemplate template;

    @Override
    public String addMoney(int user_id, Double money) {
        String sql = "UPDATE user_details SET cash = cash + ? WHERE user_id = ?";
        template.update(sql, money, user_id);
        return "Updated";
    }

    @Override
    public String withdraw(int user_id, Double money) {
        String sql1 = "SELECT cash FROM user_details WHERE user_id = ?";
        double result = template.queryForObject(sql1, Double.class, user_id);
        if (result > money) {
            String sql = "UPDATE user_details SET cash = cash - ? WHERE user_id = ?";
            template.update(sql, money, user_id);
            return "Updated";
        } else
            return "Cannot withdrawn due to insufficient funds";
    }

    @Override
    public Boolean checksufficientbalance(int userid, Double tradevalue) {
        String sql1 = "SELECT cash FROM user_details WHERE user_id = ?";
        double result = template.queryForObject(sql1, Double.class, userid);
        if (result >= tradevalue) {
            return true;
        } else
            return false;
    }

}