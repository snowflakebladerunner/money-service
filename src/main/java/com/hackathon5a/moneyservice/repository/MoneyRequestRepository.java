package com.hackathon5a.moneyservice.repository;

import org.springframework.stereotype.Component;

import com.hackathon5a.moneyservice.model.User;

@Component
public interface MoneyRequestRepository {

    public String addMoney(int user_id, Double money);

    public String withdraw(int user_id, Double money);

    public Boolean checksufficientbalance(int userid, Double tradevalue);

}
