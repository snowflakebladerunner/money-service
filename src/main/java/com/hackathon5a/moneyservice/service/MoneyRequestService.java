package com.hackathon5a.moneyservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hackathon5a.moneyservice.model.User;
import com.hackathon5a.moneyservice.repository.MoneyRequestRepository;

@Service
public class MoneyRequestService {
    @Autowired
    private MoneyRequestRepository repository;

    public String addMoney(int user_id, Double money) {
        return repository.addMoney(user_id, money);
    }

    public String withdraw(int user_id, Double money) {
        return repository.withdraw(user_id, money);
    }

    public Boolean checksufficientbalance(int userid, Double tradevalue) {
        return repository.checksufficientbalance(userid, tradevalue);
    }

}