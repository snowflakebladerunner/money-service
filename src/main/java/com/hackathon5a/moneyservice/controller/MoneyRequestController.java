package com.hackathon5a.moneyservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.hackathon5a.moneyservice.model.User;
import com.hackathon5a.moneyservice.service.MoneyRequestService;

@CrossOrigin
@RestController
@RequestMapping("/api/money")
public class MoneyRequestController {

    @Autowired
    MoneyRequestService service;

    @GetMapping(value = "/add/{user_id}/{money}")
    public String addMoney(@PathVariable int user_id, @PathVariable Double money) {
        return service.addMoney(user_id, money);
    }

    @GetMapping(value = "/withdraw/{user_id}/{money}")
    public String withdraw(@PathVariable int user_id, @PathVariable Double money) {
        return service.withdraw(user_id, money);
    }

    @GetMapping(value = "/checksufficientbalance/{userid}/{tradevalue}")
    public Boolean checksufficientbalance(@PathVariable int userid, @PathVariable Double tradevalue) {
        return service.checksufficientbalance(userid, tradevalue);
    }

}
